<?php

class Grafico{
	public function getDataSet($corPrimaria="", $porc=""){
		$dataset = "{\n
					label: \"My First dataset\",\n
					fillColor : \"".$corPrimaria."\",\n
					strokeColor : \"".$corPrimaria."\",\n
					pointColor : \"".$corPrimaria."\",\n
					pointStrokeColor : \"#fff\",\n
					pointHighlightFill : \"#fff\",\n
					pointHighlightStroke : \"".$corPrimaria."\",\n
					data : [".$porc."]
				},";
		return $dataset;		
	}
}