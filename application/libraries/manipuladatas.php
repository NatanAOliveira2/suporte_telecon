<?php
class ManipulaDatas{
	public function formatarDatas($data, $formato){
		if(strtotime($data) > 0){			
			return date($formato, strtotime($data));
		}else{
			return null;
		}
	}
}