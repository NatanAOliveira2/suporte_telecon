<!DOCTYPE html>

<html lang="pt-BR">
	<head>
		<meta charset="utf-8">		
		<link rel="stylesheet" href="<?=base_url("public/_css/normalize.css")?>" type="text/css">
		<link rel="stylesheet" href="<?=base_url("public/_bootstrap/css/temas/bootstrap-superhero.css")?>" type="text/css">
		<link rel="stylesheet" href="<?=base_url("public/_css/style.css")?>" type="text/css">
		<title>Ação não realizada com sucesso!</title>
	</head>
	<body>
		<section class="container">
			<div class="alert alert-danger">
				Ação não realizada com sucesso!
			</div>
		</section>
	</body>
</html>