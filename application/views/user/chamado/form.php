<!DOCTYPE html>

<html lang="pt-BR">
	<head>
		<meta charset="utf-8">
		<title>Abertura de Chamados - Suporte Telecon</title>
		<link rel="stylesheet" href="<?=base_url("public/_css/normalize.css")?>">
		<link rel="stylesheet" href="<?=base_url("public/_bootstrap/css/temas/bootstrap-flatly.css")?>">
		<link rel="stylesheet" href="<?=base_url("public/_css/style.css")?>">
		<style type="text/css">
			div{
				margin: 5px 0px;
			}

			.container{
				position: fixed;
				top: 50%;
				left: 50%;
				width: 972px;
				height: 480px;
				margin-left: -486px;
				margin-top: -240px;
			}
		</style>
	</head>

	<body>
		<section class="container">
			<fieldset>
				<legend>
					Abertura de chamados
				</legend>
				<form action="<?=base_url("user/chamado/form/inserir")?>" method="post" class="row">
					<div class="col-md-12">
						<label>
							Motivo:
						</label>
						<input type="text" name="motivo" class="form-control" required placeholder="Digite aqui o motivo do problema" value="<?=@$chamado->ch_motivo?>">
					</div>

					<div class="col-md-6">
						<label>
							Tipo de problema:
						</label>
						<select name="tipo_problema" class="form-control">
							<option>SELECIONE UMA OPÇÃO</option>
							<?php
								foreach ($tipo_problema as $value) {
							?>
								<option value="<?=$value->tp_id?>">
									<?=$value->tp_nome?>
								</option>
							<?php
								}
							?>
						</select>
					</div>


					<div class="col-md-6">
						<label>
							Nível do chamado:
						</label>
						<select name="nivel_chamado" class="form-control">
							<option>SELECIONE UMA OPÇÃO</option>
							<?php
								foreach ($nivel_chamado as $value) {
							?>
								<option value="<?=$value->nc_id?>">
									<?=$value->nc_nome?>
								</option>
							<?php
								}
							?>
						</select>
					</div>

					<div class="col-md-12">
						<label>
							Descrição:
						</label>
						<textarea name="descricao" rows="7" class="form-control" required ><?=@$chamado->ch_desc?></textarea>
					</div>

					<div class="col-md-12">
						<input type="submit" class="btn btn-primary">
						<input type="reset" class="btn btn-default btn-sm">
					</div>

				</form>
			</fieldset>
		</section>
	</body>
</html>