<!DOCTYPE html>
<html lang="pt-BR">
	<head>
		<meta charset="utf-8">
		<title>Bem-vindo ao sistema de suporte Telecon</title>
		<link rel="stylesheet" href="<?=base_url("public/_css/normalize.css")?>">
		<link rel="stylesheet" href="<?=base_url("public/_bootstrap/css/temas/bootstrap-superhero.css")?>">
		<link rel="stylesheet" href="<?=base_url("public/_css/style.css")?>">
		<script type="text/javascript" src="<?=base_url("public/_js/Chart/Chart.min.js")?>"></script>
		<script type="text/javascript" src="<?=base_url("public/_js/script.js")?>"></script>
	</head>

	<body>	
		<section class="container">
			<section class="container-panel">
				<nav class="navbar navbar-default">
				  <div class="navbar-header">
				    <p class="navbar-brand"><?=$user['email']?></p>
				  </div>

				  <ul class="nav navbar-nav navbar-right">
				      <li class="dropdown open">
				        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Opções <b class="caret"></b></a>
				        <ul class="dropdown-menu">
				          <li><a href="<?=base_url("user/chamado/form/index")?>">Abrir um chamado</a></li>
				          <li><a href="<?=base_url("user/inscricao/form/editarForm")?>">Configurações</a></li>
				          <li><a href="#">Sair</a></li>
				        </ul>
				      </li>
				    </ul>
				</nav>
				<div class="content">
					<div class="content-chart">					
						<div>
							<canvas id="chart" height="450" width="600"></canvas>
						</div>
						<?php
							foreach ($status_chamado as $value) {
						?>
							<div class="label" style="background-color: <?=$value->sc_cor?>;">
								<?=$value->sc_nome?>
							</div>
						<?php
							}
						?>
						
					</div>	
				</div>
		</div>

			</section>

			<section class="container-list">
				<div>
					<?php

						@$row = count($chamado);
						if($row>0){
						foreach ($chamado as $dados) {
							$data = $this->manipuladatas->formatarDatas($dados->ch_data, "d/m/Y");
					?>
					<dl class="list-item easeInOut" style="border-left: 4px solid <?=$dados->sc_cor?>;">
						<dt class="item-title easeInOut">
							<a href="#"><?=$dados->ch_motivo?></a>
						</dt>
						<dd class="item-description">	
							<?=$dados->ch_desc?>
						</dd>						
						<dd class="item-description">	
							<strong>
								Tipo de problema: <?=$dados->tp_nome?>
							</strong>
						</dd>
						<dd class="item-description">
							<strong>
								Nível do chamado: <?=$dados->nc_nome?>
							</strong>
						</dd>
						<dd>
							<div class="label label-default">
								<?=$data?> às <?=$dados->ch_hora?>
							</div>
							<div class="label" style="background-color: <?=$dados->sc_cor?>;">
								<?=$dados->sc_nome?>
							</div>
						</dd>
					</dl>
					<?php
						}
						}else{
					?>
					<dl class="list-item easeInOut" style="border-left: 4px solid #7D9DC1;">
						<dt class="item-title easeInOut">					
							<h3>	
								Nenhum chamado registrado.
							</h3>
						</dt>						
					</dl>
					<?php		
						}
					?>					
				</div>
			</section>
		</section>

		<script type="text/javascript" src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
		<script type="text/javascript" src="http://bootswatch.com/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
		<script type="text/javascript">

		var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
		var lineChartData = {
			labels : [<?=$chart['label']?>],
			datasets : [				
				<?=$chart['dataset']?>
			]

		}

	window.onload = function(){
		var ctx = document.getElementById("chart").getContext("2d");
		window.myLine = new Chart(ctx).Bar(lineChartData, {
			responsive: true,
			scaleFontColor: "#DEE3E7"
		});
	}

		</script>
	</body>
</html>