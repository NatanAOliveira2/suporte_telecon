<!DOCTYPE html>

<html lang="pt-BR">
	<head>
		<meta charset="utf-8">
		<title>Inscrição - Suporte Telecon</title>
		<link rel="stylesheet" href="<?=base_url("public/_css/normalize.css")?>">
		<link rel="stylesheet" href="<?=base_url("public/_bootstrap/css/temas/bootstrap-superhero.css")?>">
		<link rel="stylesheet" href="<?=base_url("public/_css/style.css")?>">
		<style type="text/css">
			div{
				margin: 5px 0px;
			}

			.container{
				position: fixed;
				top: 50%;
				left: 50%;
				width: 600px;
				height: 253px;
				margin-left: -300px;
				margin-top: -121.5px;
			}
		</style>
	</head>

	<body>
		<?php
			$actionForm = "user/inscricao/form/inserir";
			if(!empty($action)){
				$actionForm = $action;
			}
		?>
		<section class="container">
			<fieldset>
				<legend>
					Cadastro de usuário
				</legend>
				<form action="<?=base_url($actionForm)?>" method="post" class="row">
				<div class="col-md-12">
						<label>
							Nome e Sobrenome:
						</label>
						<input type="text" name="nome" class="form-control" required placeholder="Digite aqui o seu nome e sobrenome" value="<?=@$info->us_nome?>">
					</div>

					<div class="col-md-7">
						<label>
							E-mail:
						</label>
						<input type="email" name="login" class="form-control" required placeholder="Digite aqui o seu e-mail corporativo" value="<?=@$info->us_email?>">
					</div>

					<div class="col-md-5">
						<label>
							Senha:
						</label>
						<input type="password" name="senha" class="form-control" required placeholder="Digite aqui a sua senha" value="<?=@$info->us_senha?>">
					</div>

					<div class="col-md-12">
						<input type="submit" class="btn btn-info">
						<input type="reset" class="btn btn-default btn-sm">
					</div>

				</form>
			</fieldset>
		</section>
	</body>
</html>