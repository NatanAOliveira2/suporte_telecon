<!DOCTYPE html>

<html lang="pt-BR">
	<head>
		<meta charset="utf-8">
		<title>Bem-vindo ao sistema de suporte Telecon</title>
		<link rel="stylesheet" href="<?=base_url("public/_css/normalize.css")?>">
		<link rel="stylesheet" href="<?=base_url("public/_bootstrap/css/temas/bootstrap-superhero.css")?>">
		<link rel="stylesheet" href="<?=base_url("public/_css/style.css")?>">
		<style type="text/css">
			div{
				margin: 5px 0px;
			}

			.container{
				position: fixed;
				top: 50%;
				left: 50%;
				width: 600px;
				height: 253px;
				margin-left: -300px;
				margin-top: -121.5px;
			}
		</style>
	</head>

	<body>
		<nav class="navbar">
		  <ul class="nav navbar-nav">
		      <li>
				<a href="<?=base_url("user/inscricao/form/index")?>" class="btn btn-primary">Cadastre-se</a>		        
				<a href="<?=base_url("user/senha/form/index")?>" class="btn btn-warning btn-sm">Recuperar Senha</a>		        
		      </li>
		  </ul>		
		</nav>
		<section class="container">
			<fieldset>
				<legend>
					Login
				</legend>
				<form action="<?=base_url("login/form/authentication")?>" method="post" class="row">
					<div class="col-md-12">
						<label>
							E-mail:
						</label>
						<input type="email" name="login" class="form-control" required placeholder="Digite aqui o seu e-mail corporativo">
					</div>

					<div class="col-md-12">
						<label>
							Senha:
						</label>
						<input type="password" name="senha" class="form-control" required placeholder="Digite aqui a sua senha de acesso">
					</div>

					<div class="col-md-12">
						<input type="submit" class="btn btn-info">
						<input type="reset" class="btn btn-default btn-sm">
					</div>

				</form>
			</fieldset>
		</section>
	</body>
</html>