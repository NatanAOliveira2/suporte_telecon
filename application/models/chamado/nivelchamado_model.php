<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class NivelChamado_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}

	public function inserir($dadosForm){
		$dados = array();
		foreach ($dadosForm as $key => $value){			
			$dados[$key] = $value;	
		}
		
		return $this->db->insert('nivel_chamado', $dados);
	}

	public function listar($id=0){
		$sql = "SELECT nc_id, nc_nome, nc_cor FROM nivel_chamado order by nc_cond";
		$query = $this->db->query($sql);
		$linhas = $query->result();
		return $linhas;
	}

	public function atualizar($dadosForm){
		$dados = array();
		foreach ($dadosForm as $key => $value){			
			$dados[$key] = $value;	
		}	
		$id = $dados['id'];
		unset($dados['id']);
		$this->db->update('nivel_chamado', $dados, array('nc_id' => $id));	
	}
}