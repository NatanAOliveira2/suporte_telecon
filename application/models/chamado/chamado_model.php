<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Chamado_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}

	public function inserir($dadosForm){
		$dados = array();
		foreach ($dadosForm as $key => $value){			
			$dados[$key] = $value;	
		}
		
		return $this->db->insert('chamado', $dados);
	}

	public function listar($id=0){
		$sql = "SELECT chamado.ch_motivo, chamado.ch_hora, chamado.ch_data, chamado.ch_desc, status_chamado.sc_nome, nivel_chamado.nc_nome, nivel_chamado.nc_cond, tipo_problema.tp_nome, status_chamado.sc_cor FROM chamado, status_chamado, tipo_problema, nivel_chamado WHERE chamado.sc_id_fk=status_chamado.sc_id AND chamado.tp_id_fk=tipo_problema.tp_id AND chamado.nc_id_fk=nivel_chamado.nc_id";

		if($id!=0){
			$sql .= " AND us_id_fk=".$id." " ;
		}

		$sql .= " order by ch_data DESC, nc_cond DESC, ch_hora DESC ";
		$query = $this->db->query($sql);		
		$linhas = $query->result();
		return $linhas;
	}

	public function rows_result($sql){
		$query = $this->db->query($sql);		
		$linhas = $query->result();
		return count($linhas);
	}

	public function atualizar($dadosForm){
		$dados = array();
		foreach ($dadosForm as $key => $value){			
			$dados[$key] = $value;	
		}	
		$id = $dados['id'];
		unset($dados['id']);
		$this->db->update('chamado', $dados, array('ch_id' => $id));
	}
}