<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class TipoProblema_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}

	public function inserir($dadosForm){
		$dados = array();
		foreach ($dadosForm as $key => $value){			
			$dados[$key] = $value;	
		}
		
		return $this->db->insert('tipo_problema', $dados);
	}

	public function listar($id=0){
		$sql = "SELECT tp_id, tp_nome, tp_cor FROM tipo_problema order by tp_nome";
		$query = $this->db->query($sql);
		$linhas = $query->result();
		return $linhas;
	}

	public function atualizar($dadosForm){
		$dados = array();
		foreach ($dadosForm as $key => $value){			
			$dados[$key] = $value;	
		}	
		$id = $dados['id'];
		unset($dados['id']);
		$this->db->update('tipo_problema', $dados, array('tp_id' => $id));	
	}
}