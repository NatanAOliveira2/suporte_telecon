<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class StatusChamado_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}

	public function inserir($dadosForm){
		$dados = array();
		foreach ($dadosForm as $key => $value){			
			$dados[$key] = $value;	
		}
		
		return $this->db->insert('status_chamado', $dados);
	}

	public function listar($id=0, $cond=0){
		$sql = "SELECT sc_id, sc_nome, sc_cor FROM status_chamado ";
		
		if($id!=0 || $cond!=0){
			$sql .= " WHERE ";
		}

		if($id!=0){
			$sql .= " sc_id=".$id." ";
		}

		if($cond!=0){
			$sql .= " sc_cond=".$cond." ";
		}

		$sql .= "order by sc_nome";
		$query = $this->db->query($sql);
		$linhas = $query->result();
		return $linhas;
	}

	public function atualizar($dadosForm){
		$dados = array();
		foreach ($dadosForm as $key => $value){			
			$dados[$key] = $value;	
		}	
		$id = $dados['id'];
		unset($dados['id']);
		$this->db->update('status_chamado', $dados, array('sc_id' => $id));		
	}
}