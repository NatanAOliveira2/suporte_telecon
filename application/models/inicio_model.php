<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inicio_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}

	public function inserir($dadosForm){
		$dados = array();
		foreach ($dadosForm as $key => $value){			
			$dados[$key] = $value;	
		}
		
		return $this->db->insert('postagem', $dados);
	}

	public function listar($id=0){
		$sql = "SELECT titulo, assunto FROM postagem order by titulo";
		$query = $this->db->query($sql);
		$linhas = $query->result();
		return $linhas;
	}
}