<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}

	public function inserir($dadosForm){
		$dados = array();
		foreach ($dadosForm as $key => $value){			
			$dados[$key] = $value;	
		}
		
		return $this->db->insert('usuario', $dados);
	}

	public function get($id=0, $email=""){
		$sql = "SELECT * FROM usuario, grupo_usuario WHERE usuario.gu_id_fk=grupo_usuario.gu_id ";

		if($id!=0){
			$sql .= " AND usuario.us_id=".$id." ";
		}

		if(!empty($email)){
			$sql .= " AND usuario.us_email='".$email."' ";
		}

		$sql .= "order by us_nome";

		$query = $this->db->query($sql);
		$linhas = $query->row_object();
		return $linhas;
	}

	public function getAll($id=0){
		$sql = "SELECT * FROM usuario ";
		if($id!=0 || !empty($email)){
			$sql .= " WHERE ";
		}

		if($id!=0){
			$sql .= " us_id=".$id." ";
		}

		$sql .= "order by us_nome";

		$query = $this->db->query($sql);
		$linhas = $query->result();
		return $linhas;
	}

	public function atualizar($dadosForm){
		$dados = array();
		foreach ($dadosForm as $key => $value){			
			$dados[$key] = $value;	
		}
		$where['us_id'] = "".$dados['us_id'].""; 
		unset($dados['us_id']);
		$upd = $this->db->update('usuario', $dados, $where);
		return $upd;
	}
}