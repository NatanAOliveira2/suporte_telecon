<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Grupo_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}

	public function inserir($dadosForm){
		$dados = array();
		foreach ($dadosForm as $key => $value){			
			$dados[$key] = $value;	
		}
		
		return $this->db->insert('usuario', $dados);
	}

	public function get($id=0, $nivel=0){
		$sql = "SELECT * FROM grupo_usuario ";

		if($id!=0){
			$sql .= "  WHERE gu_id=".$id." ";
		}

		$sql .= " order by gu_nome";

		$query = $this->db->query($sql);
		$linhas = $query->row_object();
		return $linhas;
	}

	public function getAll($id=0){
		$sql = "SELECT * FROM grupo_usuario  ";

		if($id!=0){
			$sql .= " WHERE gu_id=".$id." ";
		}

		$sql .= "order by gu_nome";

		$query = $this->db->query($sql);
		$linhas = $query->result();
		return $linhas;
	}

	public function atualizar($dadosForm){
		$dados = array();
		foreach ($dadosForm as $key => $value){			
			$dados[$key] = $value;	
		}
		$where['gu_id'] = "".$dados['gu_id'].""; 
		unset($dados['gu_id']);
		$upd = $this->db->update('grupo_usuario', $dados, $where);
		return $upd;
	}
}