<?php
class SessaoUser extends CI_Model{
	protected $email, $senha;

	public function set($var, $valor){
		$this->$var = $valor;
	}

	public function get($var){
		return $this->$var;
	}
	
	public function __construct(){
		$this->load->model("user/user_model");
	}

	public function verificarLogin($nivelAcesso=0, $email=""){
		$dados = $this->session->all_userdata();
		@$email = $dados['us_email'];
		@$senha = $dados['us_senha'];
		$user = $this->user_model->get(0, $email);
		$nivel = $user->gu_nivel;
		if($nivelAcesso==0){
			$nivelAcesso = $nivel;
		}
		
		if((!isset($email) && !isset($senha)) || $nivel!=$nivelAcesso){
			$this->load->view('messages/error');
			redirect('login/form/index', 'refresh');
		}

		return $dados;
	}
}