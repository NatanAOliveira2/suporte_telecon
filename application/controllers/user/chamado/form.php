<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class form extends CI_Controller{
	protected $motivo, $desc, $data, $hora, $status_chamado, $nivel_chamado, $tipo_problema, $user;

	public function set($var, $valor){
		$this->$var = $valor;
	}

	public function get($var){
		return $this->$var;
	}

	public function __construct(){
		parent::__construct();
		$this->load->model("chamado/chamado_model");
		$this->load->model("chamado/statuschamado_model");
		$this->load->model("chamado/nivelchamado_model");
		$this->load->model("chamado/tipoproblema_model");
		$sessao = $this->sessaouser->verificarLogin();
		$this->set("user", $sessao['us_id']);
	}

	public function index(){
		$dados["chamado"] = $this->chamado_model->listar();
		$dados["status_chamado"] = $this->statuschamado_model->listar();
		$dados["nivel_chamado"] = $this->nivelchamado_model->listar();
		$dados["tipo_problema"] = $this->tipoproblema_model->listar();
		$this->load->view("user/chamado/form", $dados);
	}

	public function validarDados(){
		$dados = $_POST;
		$this->set("motivo", $dados['motivo']);
		$this->set("desc", $dados['descricao']);
		$status_chamado = $this->statuschamado_model->listar(0, 1);
		foreach ($status_chamado as $status) {
			$idStatus = $status->sc_id;
		}
		$this->set("status_chamado", $idStatus);
		$this->set("nivel_chamado", $dados['nivel_chamado']);
		$this->set("tipo_problema", $dados['tipo_problema']);
		$this->set("data", date("Y-m-d"));
		$this->set("hora", date("H:i:s"));
	}

	public function inserir(){
		$this->validarDados();
		$dados['ch_motivo'] = $this->get("motivo");
		$dados['ch_desc'] = $this->get("desc");
		$dados['ch_data'] = $this->get("data");
		$dados['ch_hora'] = $this->get("hora");
		$dados['sc_id_fk'] = $this->get("status_chamado");
		$dados['nc_id_fk'] = $this->get("nivel_chamado");
		$dados['tp_id_fk'] = $this->get("tipo_problema");
		$dados['us_id_fk'] = $this->get("user");
		
		$query = $this->chamado_model->inserir($dados);

		if($query){
			$this->load->view("messages/success", $dados);
		}else{
			$this->load->view("messages/error", $dados);
		}
	}
}