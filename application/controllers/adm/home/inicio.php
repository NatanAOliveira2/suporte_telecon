<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class inicio extends CI_Controller{
	protected $email, $nome, $id, $nivelAcesso;
	public $meses = array(1=>"Jan", 2=>"Fev", 3=>"Mar", 4=>"Abr", 5=>"Mai", 6=>"Jun", 7=>"Jul", 8=>"Ago", 9=>"Set", 10=>"Out", 11=>"Nov", 12=>"Dez");

	public function set($var, $valor){
		$this->$var = $valor;
	}

	public function get($var){
		return $this->$var;
	}

	public function __construct(){
		parent::__construct();
		$this->load->model("chamado/chamado_model");
		$this->load->model("chamado/statuschamado_model");
		$this->load->model("chamado/nivelchamado_model");
		$this->load->model("chamado/tipoproblema_model");
		$this->load->library("manipuladatas");
		$this->load->library("grafico");
		$this->load->model("user/user_model");
		$sessao = $this->sessaouser->verificarLogin(2);
		$this->set("email", $sessao['us_email']);
		$this->set("senha", $sessao['us_senha']);
		$this->set("id", $sessao['us_id']);
		$this->set("nivelAcesso", $sessao['gu_nivel']);
	}

	public function index(){		
		$dados["user"]['nome'] = $this->get("nome");
		$dados["user"]['email'] = $this->get("email");

		$dados["chamado"] = $this->chamado_model->listar($this->get("id"));
		$dados["status_chamado"] = $this->statuschamado_model->listar();
		$dados["nivel_chamado"] = $this->nivelchamado_model->listar();
		$dados["tipo_problema"] = $this->tipoproblema_model->listar();
		$label = $this->getMeses();
		$estatistica = $this->estatisticaChamado();
		$dados["chart"]['label'] = $label;
		$dados["chart"]['dataset'] = $estatistica;
		$this->load->view("user/home/inicio", $dados);
	}

	public function estatisticaChamado(){
		$meses = $this->get("meses");	
		$ano = date("Y");
		$claus = array();
		$dataset = "";

		$statusChamado = $this->statuschamado_model->listar();
			
			foreach ($statusChamado as $status) {
				$porc = "";
				foreach ($meses as $mes=>$value) {		
					$dataInicial = $ano."-".$mes."-01";
					$dataFinal = $ano."-".$mes."-31";

					$sql = "SELECT chamado.ch_id FROM chamado, status_chamado, tipo_problema, nivel_chamado WHERE chamado.sc_id_fk=status_chamado.sc_id AND chamado.tp_id_fk=tipo_problema.tp_id AND chamado.nc_id_fk=nivel_chamado.nc_id AND us_id_fk=".$this->get("id")." AND ch_data BETWEEN '".$dataInicial."' AND '".$dataFinal."'";
						$sql .= " order by ch_data DESC, nc_cond DESC, ch_hora DESC ";
						$total = $this->chamado_model->rows_result($sql);

					$sql = "SELECT chamado.ch_id FROM chamado, status_chamado, tipo_problema, nivel_chamado WHERE chamado.sc_id_fk=status_chamado.sc_id AND chamado.tp_id_fk=tipo_problema.tp_id AND chamado.nc_id_fk=nivel_chamado.nc_id AND us_id_fk=".$this->get("id")." AND status_chamado.sc_id=".$status->sc_id." AND ch_data BETWEEN '".$dataInicial."' AND '".$dataFinal."'";
					$sql .= " order by ch_data DESC, nc_cond DESC, ch_hora DESC ";
					$rows = $this->chamado_model->rows_result($sql);
					@$estat = number_format(($rows*100)/$total,2);
					$porc .= $estat . ",";
				}				
				$dataset .= $this->grafico->getDataSet($status->sc_cor, $porc);
			}
		return $dataset;	
	}

	public function getMeses(){
		$meses = $this->get("meses");	
		$label="";

		foreach ($meses as $value) {
			$label .= "\"$value\", ";
		}

		return $label;
	}

}