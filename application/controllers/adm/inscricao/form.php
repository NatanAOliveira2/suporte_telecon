<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class form extends CI_Controller{
	protected $nome, $email, $senha, $id;

	public function set($var, $valor){
		$this->$var = $valor;
	}

	public function get($var){
		return $this->$var;
	}

	public function __construct(){
		parent::__construct();
		$this->load->model("user/user_model");
		$sessao = $this->sessaouser->verificarLogin();
		$this->set("id", $sessao['us_id']);
	}

	public function index(){
		$this->load->view("user/inscricao/form");
	}
	
	public function editarForm(){
		@$linha = $this->user_model->get($this->get("id"));
		$dados["info"] = $linha;
		$dados["action"] = "user/inscricao/form/atualizarDados";
		$this->load->view("user/inscricao/form", $dados);
	}

	public function inserir(){
		$dados = $_POST;
		$this->set("nome", $dados['nome']);
		$this->set("email", $dados['login']);
		$this->set("senha", $dados['senha']);
		
		$user['us_nome'] = $this->get('nome');
		$user['us_email'] = $this->get('email');
		$user['us_senha'] = $this->get('senha');

		$inserir = $this->user_model->inserir($user);

		if($inserir==1){
			$this->load->view('messages/success');
		}
	}

	public function atualizarDados(){
		$dados = $_POST;
		$this->set("nome", $dados['nome']);
		$this->set("email", $dados['login']);
		$this->set("senha", $dados['senha']);
		
		$user['us_id'] = $this->get('id');
		$user['us_nome'] = $this->get('nome');
		$user['us_email'] = $this->get('email');
		$user['us_senha'] = $this->get('senha');

		$atualizar = $this->user_model->atualizar($user);
		if($atualizar==1){
			$this->load->view('messages/success');
		}
	}

}