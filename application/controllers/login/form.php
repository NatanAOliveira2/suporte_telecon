<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class form extends CI_Controller{
	protected $email, $senha;

	public function set($var, $valor){
		$this->$var = $valor;
	}

	public function get($var){
		return $this->$var;
	}

	public function __construct(){
		parent::__construct();
		$this->load->model("user/user_model");
	}

	public function index(){
		$this->load->view("login/form");
	}

	public function authentication(){
		$dados = $_POST;
		$this->set("email", $dados['login']);
		$this->set("senha", $dados['senha']);
		$email = $this->get("email");
		$senha = $this->get("senha");
		$linha = $this->user_model->get(0, $email);
		$result = count($linha);
		$status=1;

		if($result>0){
			$senha_user = $linha->us_senha;			
			if($senha==$senha_user){
				$this->session->set_userdata($linha);
			}else{
				$status=0;
			}
		}else{
			$status=0;
		}

		@$nivel = $linha->gu_nivel;
		if($nivel==1){
			$this->load->view('messages/success');

		}elseif($nivel==2){
			redirect('adm', 'refresh');

		}else{
			$this->load->view('messages/error');
			redirect('login', 'refresh');
		}
	}

}