CREATE DATABASE suporte_telecon;
USE suporte_telecon;

CREATE TABLE IF NOT EXISTS  `ci_sessions` (
	session_id varchar(40) DEFAULT '0' NOT NULL,
	ip_address varchar(45) DEFAULT '0' NOT NULL,
	user_agent varchar(120) NOT NULL,
	last_activity int(10) unsigned DEFAULT 0 NOT NULL,
	user_data text NOT NULL,
	PRIMARY KEY (session_id),
	KEY `last_activity_idx` (`last_activity`)
);

CREATE TABLE grupo_usuario(
	gu_id INTEGER(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	gu_nome VARCHAR(55) NOT NULL,
	gu_nivel SMALLINT(1) NOT NULL
);

CREATE TABLE usuario(
	us_id INTEGER(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	gu_id_fk INTEGER(11) NOT NULL,
	us_nome VARCHAR(55) NOT NULL,
	us_email VARCHAR(75) NOT NULL,
	us_senha VARCHAR(55) NOT NULL,
	CONSTRAINT fk_gu FOREIGN KEY (gu_id_fk) REFERENCES grupo_usuario(gu_id)
);


CREATE TABLE nivel_chamado(
	nc_id INTEGER(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	nc_nome VARCHAR(55) NOT NULL,
	nc_cor VARCHAR(23) NOT NULL,
	nc_cond SMALLINT(1) NOT NULL 
)ENGINE=InnoDB;

CREATE TABLE tipo_problema(
	tp_id INTEGER(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	tp_nome VARCHAR(55) NOT NULL,
	tp_cor VARCHAR(23) NOT NULL,
	tp_cond SMALLINT(1) NOT NULL 
)ENGINE=InnoDB;

CREATE TABLE status_chamado(
	sc_id INTEGER(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	sc_nome VARCHAR(55) NOT NULL,
	sc_cor VARCHAR(23) NOT NULL,
	sc_cond SMALLINT(1) NOT NULL 
)ENGINE=InnoDB;

CREATE TABLE chamado(
	ch_id INTEGER(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	tp_id_fk INTEGER(11) NOT NULL,
	nc_id_fk INTEGER(11) NOT NULL,
	sc_id_fk INTEGER(11) NOT NULL,
	us_id_fk INTEGER(11) NOT NULL,
	ch_motivo VARCHAR(55) NOT NULL,
	ch_data DATE NOT NULL,
	ch_hora TIME NOT NULL,
	ch_desc TEXT NOT NULL,
	CONSTRAINT fk_tp FOREIGN KEY (tp_id_fk) REFERENCES tipo_problema(tp_id),
	CONSTRAINT fk_nc FOREIGN KEY (nc_id_fk) REFERENCES nivel_chamado(nc_id),
	CONSTRAINT fk_us FOREIGN KEY (us_id_fk) REFERENCES usuario(us_id),
	CONSTRAINT fk_sc FOREIGN KEY (sc_id_fk) REFERENCES status_chamado(sc_id)
)ENGINE=InnoDB;

CREATE TABLE resposta(
	res_id INTEGER(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	ch_id_fk INTEGER(11) NOT NULL,
	res_titulo VARCHAR(75) NOT NULL,
	res_texto TEXT NOT NULL,
	res_data DATE NOT NULL,
	res_hora TIME NOT NULL,
	CONSTRAINT fk_ch FOREIGN KEY (ch_id_fk) REFERENCES chamado(ch_id)
);
